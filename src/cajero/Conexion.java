package cajero;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Conexion {
	private ObjectContainer db = null;
	
	/*
	 *	Creacion de la conexion. 
	 */
	private void openConexion(){
		db = Db4oEmbedded.openFile("Principal");
	}
	
	/*
	 * 	Cierre de la conexion.
	 */
	private void closeConexion(){
		db.close();
	}
	
	/*
	 * 	Insercion de datos en la base de datos.
	 */
	void insertUsuario(Usuario u){
		openConexion();
		db.store(u);
		closeConexion();
	}
	
	/*
	 * 	Select de los usuarios.
	 */
	public List<Usuario> selectUsuario(){
		openConexion();
		ObjectSet listarUsuarios = db.queryByExample(Usuario.class);
		List<Usuario> us = new ArrayList<>();
		
		for(Object listarUsuario : listarUsuarios){
			us.add((Usuario) listarUsuario);
		}
		
		closeConexion();
		
		return us;
	}
	
	public Usuario selectUsuario(Usuario u){
		openConexion();
		ObjectSet resul = db.queryByExample(u);
		Usuario usuario = (Usuario) resul.next();
		closeConexion();
		
		return usuario;
	}
	
	public void updateUsuario(String tarjeta, String nom, String apeP, String apeM, int nip, float fondos){
		openConexion();
		Usuario u = new Usuario();
		u.setTarjeta(tarjeta);
		ObjectSet result = db.queryByExample(u);
		Usuario preU = (Usuario) result.next();
		preU.setNombre(nom);
		preU.setApellidoP(apeP);
		preU.setApellidoM(apeM);
		preU.setNip(nip);
		preU.setFondos(fondos);
		db.store(preU);
		closeConexion();
	}
	
	public void updateFondo(String tarjeta, float fondos){
		openConexion();
		Usuario u = new Usuario();
		u.setTarjeta(tarjeta);
		ObjectSet result = db.queryByExample(u);
		Usuario preU = (Usuario) result.next();
		preU.setFondos(fondos);
		db.store(preU);
		closeConexion();
	}
	
	public void deleteUsuario(String tarjeta){
		openConexion();
		Usuario u = new Usuario();
		u.setTarjeta(tarjeta);
		ObjectSet result = db.queryByExample(u);
		Usuario preU = (Usuario) result.next();
		db.delete(preU);
		closeConexion();
	}
}
