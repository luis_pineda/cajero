package cajero;

public class Usuario {
	private String tarjeta;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private int nip;
	private float fondos;
	
	public void setTarjeta(String tarjeta){
		this.tarjeta = tarjeta;
	}
	
	public String getTarjeta(){
		return tarjeta;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public void setApellidoP(String apellidoP){
		this.apellidoP = apellidoP;
	}
	
	public String getApellidoP(){
		return apellidoP;
	}
	
	public void setApellidoM(String apellidoM){
		this.apellidoM = apellidoM;
	}
	
	public String getApellidoM(){
		return apellidoM;
	}
	
	public void setNip(int nip){
		this.nip = nip;
	}
	
	public int getNip(){
		return nip;
	}
	
	public void setFondos(float fondos){
		this.fondos = fondos;
	}
	
	public float getFondos(){
		return fondos;
	}
	
	public String toString() {
		return "Usuario("+"Numero tarjeta: " + tarjeta + "\n" +
				"nombrePersona: " + nombre + "\n" +
				"apellidoPPersona: " + apellidoP + "\n" +
				"apellidoMPersona: " + apellidoM + "\n" +
				"nip: "+ nip + "\n" +
				"fondos: " + fondos;
	}
}
