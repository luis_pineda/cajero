package cajero;

import java.util.Scanner;

public class Principal {
	public static void main(String[]args)
	{
		String n;
		float dinero, cantidad, fondos;
		int nip;
		boolean decision;
		
		Conexion c = new Conexion();
		Usuario u = new Usuario();
		
		System.out.println("Bienvenido, favor de ingresar el numero de tarjeta");
		Scanner lec = new Scanner(System.in);
		n = lec.next();
		
		System.out.println("Bienvenido, favor de ingresar el nip");
		nip = lec.nextInt();
		
		u.setTarjeta(n);
		u.setNip(nip);
		u = c.selectUsuario(u);
		
		String tarjeta = u.getTarjeta();
		
		if(u.getTarjeta().equalsIgnoreCase("1234567890123456"))
		{
			do{
				decision = false;
				
				System.out.println("Bienvenido usuario administrador. Selecciona la opcion: \n"
						+ "Fondos disponibles: " + u.getFondos() + "\n"
						+ "1)	Registrar otro usuario\n"
						+ "2)	Salir");
				nip = lec.nextInt();
				
				switch(nip)
				{
					case 1:
						System.out.println("A continuacion ingresa los datos para la creacion del usuario");
						
						System.out.print("Nombre(s): ");
						n = lec.next();
						u.setNombre(n);
						System.out.print("Apellido Paterno: ");
						n = lec.next();
						u.setApellidoP(n);
						System.out.print("Apellido Materno: ");
						n = lec.next();
						u.setApellidoM(n);
						System.out.print("Numero de tarjeta: ");
						n = lec.next();
						u.setTarjeta(n);
						System.out.print("NIP: ");
						nip = lec.nextInt();
						u.setNip(nip);
						System.out.print("Fondos: ");
						dinero = lec.nextFloat();
						u.setFondos(dinero);
						
						c.insertUsuario(u);
						System.out.println(u);
						break;
					case 2:
						System.out.println("Gracias por su preferencia");
						System.exit(0);
						break;
						default:
							System.out.println("Gracias por su preferencia");
							System.exit(0);
							break;
				}
				
				System.out.println("�Deseas insertar otro usuario? S/N");
				n = lec.next();
				
				if(n.equalsIgnoreCase("s"))
				{
					decision = true;
				}
			}while(decision == true);
		}
		else
		{
			System.out.println("Bienvenido " + u.getNombre() + " " + u.getApellidoP() + " " +u.getApellidoM());
			do{
				decision = false;
				
				System.out.println("Selecciona una opcion: \n"
						+ "Fondos disponibles: " + u.getFondos() + "\n"
						+ "1)	Retiro\n"
						+ "2)	Deposito" + "\n"
						+ "3)	Salir");
				nip = lec.nextInt();
				
				switch(nip)
				{
					case 1:
						System.out.println("Inserta cuando dinero deseas retirar");
						cantidad = lec.nextFloat();
						if(u.getFondos() >= cantidad)
						{
							dinero = u.getFondos() - cantidad;
							u.setFondos(dinero);
							c.updateFondo(tarjeta, dinero);
							System.out.println("Has retirado " + cantidad);
						}
						break;
					case 2:
						System.out.println("Inserta cuando dinero deseas depositar");
						cantidad = lec.nextFloat();		
						dinero = u.getFondos() + cantidad;
						u.setFondos(dinero);
						c.updateFondo(tarjeta, dinero);
						System.out.println("Has depositado " + cantidad);
						break;
					case 3:
						System.out.println("Gracias por su preferencia");
						System.exit(0);
						break;
						default:
							System.out.println("Gracias por su preferencia");
							System.exit(0);
							break;
				}
				
				System.out.println("�Deseas realizar otra operacion? S/N");
				n = lec.next();
				
				if(n.equalsIgnoreCase("s"))
				{
					decision = true;
				}
			}while(decision == true);
		}
	}
}
